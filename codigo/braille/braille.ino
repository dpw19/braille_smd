// Ultima modificacion 10/11/2020

// Código de prueba para el cuadratin braille smd
// Puedes encontrar el esquemático y pcb y este
// código en gitlab.com/dpw/brailleSMD
//
// Este programa es software libre, puedes utilizarlo, estudiarlo, mejorarlo,
// compartirlo bajo los términos de la Licencia Publica General GNU/GPL
//
  
// Pines del cuadratin braille
const int pins[6] = {8,9,10,11,12,13};

// Codificacion de los caracteres braille
// To Do: Terminar la tabla 
const byte puntosCuadratin[10] = {
                          0b100000, // a
                          0b110000, // b
                          0b100100, // c
                          0b100110, // d
                          0b100010, // e     
                          0b110100, // f
                          0b110110, // g
                          0b110010, // h
                          0b010100, // i
                          0b010110, // j
};


void setup() {
// Inicializa los pines del cuadratin como salida
  for(int i = 0; i < 6; i++) {
    pinMode(pins[i], OUTPUT);  
  }

// Apaga los segmentos del cuadratin
  lightSegments(0);
}

void loop() {
// recorre la matriz punto cuadratin
  for(int i = 0; i < 10; i++) {
    lightSegments(i);
    delay(1000);
  }
}

void lightSegments(int number) {
  byte numberBit = puntosCuadratin[number];
  for (int i = 0; i < 6; i++)  {
    int bit = bitRead(numberBit, i);
    digitalWrite(pins[i], bit);
  }
}
